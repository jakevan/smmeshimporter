package me.jakev.smmeshimporter;

/**
 * Created by Jake on 11/30/2020.
 * <insert description here>
 */
public class Templates {
    public static final String scene =
                    "<scene application=\"3DS Max\" author=\"StarLoader Model Importer\" formatVersion=\"1.0\" minOgreVersion=\"1.8\" ogreMaxVersion=\"2.6.1\" unitType=\"/10\" unitsPerMeter=\"3.93701\" upAxis=\"y\">\n" +
                    "    <environment>\n" +
                    "        <colourAmbient b=\"0.333333\" g=\"0.333333\" r=\"0.333333\"/>\n" +
                    "        <colourBackground b=\"0\" g=\"0\" r=\"0\"/>\n" +
                    "        <clipping far=\"10000\" near=\"0\"/>\n" +
                    "    </environment>\n" +
                    "    <nodes>\n" +
                    "        <node name=\"$MESHNAME\">\n" +
                    "            <position x=\"0\" y=\"0\" z=\"0\"/>\n" +
                    "            <scale x=\"1\" y=\"1\" z=\"1\"/>\n" +
                    "            <rotation qw=\"1\" qx=\"0\" qy=\"0\" qz=\"0\"/>\n" +
                    "            <entity castShadows=\"true\" meshFile=\"$MESHNAME.mesh\" name=\"$MESHNAME\" receiveShadows=\"true\">\n" +
                    "                <subentities>\n" +
                    "                    <subentity index=\"0\" materialName=\"$MATERIALNAME\"/>\n" +
                    "                </subentities>\n" +
                    "            </entity>\n" +
                    "        </node>\n" +
                    "    </nodes>\n" +
                    "</scene>";


    public static final String meshXML =
                    "<mesh>\n" +
                    "    <submeshes>\n" +
                    "        <submesh material=\"$MATERIALNAME\" usesharedvertices=\"false\" use32bitindexes=\"false\" operationtype=\"triangle_list\">\n" +
                    "$FACES\n" +
                    "            <geometry vertexcount=\"$VERTEXCOUNT\">\n" +
                    "                <vertexbuffer positions=\"true\" normals=\"true\" texture_coord_dimensions_0=\"float2\" tangents=\"false\" binormals=\"false\" texture_coords=\"1\">\n" +
                    "$VERTEXBUFFER\n" +
                    "                </vertexbuffer>\n" +
                    "            </geometry>\n" +
                    "        </submesh>\n" +
                    "    </submeshes>\n" +
                    "    <submeshnames>\n" +
                    "        <submeshname name=\"submesh0\" index=\"0\"/>\n" +
                    "    </submeshnames>\n" +
                    "</mesh>";

    //TODO: Proper support for materials. Edit the exported file yourself if need be
    public static final String material =
        "material $MATERIALNAME\n" +
        "{\n" +
        "\ttechnique\n" +
        "\t{\n" +
        "\t\tpass\n" +
        "\t\t{\n" +
        "\t\t\tambient 0.588 0.588 0.588 1\n" +
        "\t\t\tdiffuse 0.588 0.588 0.588 1\n" +
        "\t\t\tspecular 0 0 0 1 10\n" +
        "\t\t\tscene_blend one one_minus_src_alpha\n" +
        "\n" +
        "\t\t\ttexture_unit\n" +
        "\t\t\t{\n" +
        "\t\t\t\ttexture $MATERIALNAME.png\n" +
        "\t\t\t}\n" +
        "\t\t}\n" +
        "\n" +
        "\t}\n"
            ;

    public static String getScene(String meshName, String materialName){
        return scene.replace("$MESHNAME", meshName).replace("$MATERIALNAME", materialName);
    }
    public static String getMeshXML(String meshName, String materialName, String vertexBuffer, int vertexCount, String faces){
        return meshXML
                .replace("$MATERIALNAME", materialName)
                .replace("$MESHNAME", meshName)
                .replace("$VERTEXCOUNT", String.valueOf(vertexCount))
                .replace("$FACES", faces)
                .replace("$VERTEXBUFFER", vertexBuffer);
    }
    public static String getMaterial(String matName){
        return material.replace("$MATERIALNAME", matName);
    }
}
