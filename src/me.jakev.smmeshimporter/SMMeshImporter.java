package me.jakev.smmeshimporter;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Jake on 11/30/2020.
 * <insert description here>
 */
public class SMMeshImporter {
    public static void main(String[] args) {
        System.out.println("Running SMMeshImporter");
        File[] files = new File("input").listFiles();
        String meshName = null;
        File sceneFile = null;
        File meshXML = null;
        File materialFile = null;
        String oldMatName = null;
        ArrayList<File> toCopy = new ArrayList<>();
        for (File file : files) {
            String fName = file.getName();
            if (fName.endsWith(".scene")) {
                sceneFile = file;
                meshName = fName.substring(0, fName.length() - ".scene".length());
            } else if (fName.endsWith(".xml")) {
                meshXML = file;
            } else if (fName.endsWith(".material")) {
                materialFile = file;
                oldMatName = fName.replace(".material", "");
            }else if(fName.endsWith(".png") || fName.endsWith(".jpg") || fName.endsWith(".tga")){
                toCopy.add(file);
            }
        }
        System.out.println("Mesh Name: " + meshName + ", sceneFile: " + sceneFile + ", meshXML: " + meshXML);
        String vVertexBuffer = "";
        String faces = null;
        int vertexCount = 0;
        try {
            Scanner xmlScanner = new Scanner(meshXML);
            while (xmlScanner.hasNext()) {
                String line = xmlScanner.nextLine();

                //Check for vertex stuff
                if (line.contains("<vertex>")) {
                    StringBuilder result = new StringBuilder(line).append("\n");
                    while (true) {
                        String next = xmlScanner.nextLine();
                        if (next.contains("</vertexbuffer>")) {
                            break;
                        } else {
                            result.append(next).append("\n");
                        }
                    }
                    vVertexBuffer = result.toString();
                }

                //Check for vertex count
                if (line.contains("vertexcount=")) {
                    vertexCount = Integer.parseInt(line.split("\"")[1]);
                }

                //Faces
                if (line.contains("<faces count=")) {
                    StringBuilder res = new StringBuilder(line).append("\n");
                    while (true) {
                        String s = xmlScanner.nextLine();
                        res.append(s).append("\n");
                        if (s.contains("</faces>")) {
                            break;
                        }
                    }
                    faces = res.toString();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(vVertexBuffer);

        System.out.println(faces);
        String sceneFileResult = Templates.getScene(meshName, meshName);
        String xmlFileResult = Templates.getMeshXML(meshName, meshName, vVertexBuffer, vertexCount, faces);
        String materialResult = Templates.getMaterial(meshName);

        try {
            new File("tmp").mkdir();
            File file = new File("tmp/" + meshName + ".scene");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(sceneFileResult.getBytes());
            fileOutputStream.close();

            fileOutputStream = new FileOutputStream(new File("tmp/" + meshName + ".mesh.xml"));
            fileOutputStream.write(xmlFileResult.getBytes());
            fileOutputStream.close();

            fileOutputStream = new FileOutputStream(new File("tmp/" + meshName + ".material"));
            fileOutputStream.write(materialResult.getBytes());
            fileOutputStream.close();

            for (File f : toCopy) {
                Path from = f.toPath();
                Path to = Paths.get("tmp/" + f.getName());
                Files.copy(from, to);
            }
            ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(meshName + ".zip"));
            for (File f : new File("tmp").listFiles()){
                f.deleteOnExit();
                ZipEntry entry = new ZipEntry(f.getName());
                zip.putNextEntry(entry);
                byte[] bytes = Files.readAllBytes(f.toPath());
                zip.write(bytes, 0 , bytes.length);
                zip.closeEntry();
            }
            zip.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
