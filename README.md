# SMMeshImporter

Convert ogre3d meshes from blender2ogre to the exact format that StarMade requires.

# HOW TO USE
Go into the "working" folder
Put your exported blender files into input.

Run meshimporter.jar

There you go, you have your zip file

# What does it do?
StarMade mesh files must conform to an extremely rigid structure, so simply exporting them to OGRE is not enough.
Files must be named the exact same as their mesh, .material must use tabs instead of spaces, and more.